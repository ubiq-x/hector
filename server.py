import pickle
import pigpio

import tcp.server

from servo_gpio import Servo


# ======================================================================================================================
class HectorRequestHandler(tcp.server.RequestHandler):
    
    # ------------------------------------------------------------------------------------------------------------------
    def cmd_cam_go_center(self):
        self.server._servos[Servo.IDX_NA].servo.go_cen()
        self.server._servos[Servo.IDX_NR].servo.go_cen()
        return None
    
    # ------------------------------------------------------------------------------------------------------------------
    def cmd_cam_go_to(self, a, r):
        self.server._servos[Servo.IDX_NA].servo.set_rad(a)
        self.server._servos[Servo.IDX_NR].servo.set_rad(r)
        return None
    
    # ------------------------------------------------------------------------------------------------------------------
    def cmd_cam_get_servo_inf(self):
        s = self.server._servos
        return '{} {} 0'.format(s[Servo.IDX_NA].servo.PWM_COR, s[Servo.IDX_NR].servo.PWM_COR)
    
    # ------------------------------------------------------------------------------------------------------------------
    def do_cmd(self, cmd, args):
        # print('{} {}'.format(cmd, args))
        
        try:
            return {
                # cmd:      13cam-go-center
                # cmd:      17cam-get-servo-inf
                
                'cam-go-center'     : lambda: self.cmd_cam_go_center(),
                'cam-go-to'         : lambda: self.cmd_cam_go_to(float(args[0]), float(args[1])),
                'cam-get-servo-inf' : lambda: self.cmd_cam_get_servo_inf()
            }.get(cmd, lambda: None)()  # unknown command; None instead of self.RET_CMD_ERR suppreses response to client
        except Exception as e:
            print('{} {}'.format(cmd, args))
            print(e)


    # ------------------------------------------------------------------------------------------------------------------
    def handle(self):
        tcp.server.RequestHandler.handle(self)
        self.cmd_cam_go_center()


# ======================================================================================================================
class HectorServer(tcp.server.TCPServer):
    PORT = 3333
    
    _pi     = None
    _servos = None
    
    # ------------------------------------------------------------------------------------------------------------------
    def __init__(self, server_address, RequestHandlerClass, bind_and_activate=True):
        import time
        
        try:
            # (1) Init the Pi:
            self._pi = pigpio.pi()
            if not self._pi.connected: exit(1)
            
            # (2) Init servos:
            self._servos = Servo.get_servos(self._pi)
            
            # (3) Indicate readiness by moving some servos:
            self._servos[Servo.IDX_NR].servo.go_cen().go_oc(250, True).go_cen()
        except:
            if self._pi.connected:
                self._pi.stop()
        
        tcp.server.TCPServer.__init__(self, server_address, RequestHandlerClass, bind_and_activate, True)


# ======================================================================================================================
if __name__ == '__main__':
    import signal
    import sys
    
    s = HectorServer(('192.168.0.182', HectorServer.PORT), HectorRequestHandler)
    
    signal.signal(signal.SIGINT, lambda signal, frame: sys.exit(0))  # register CTRL+C event
    
    s.serve_forever()
