#
# TODO
#     Consider using these to set PWM:
#         abyz.me.uk/rpi/pigpio/python.html#set_servo_pulsewidth
#         abyz.me.uk/rpi/pigpio/python.html#set_PWM_range
#         abyz.me.uk/rpi/pigpio/python.html#set_PWM_frequency
#     The camera azimuth servo experiences drift very quickly. Diagnose and fix the problem.
#         - One possible reason might be that the angle updates are too fine. Try adding a smoothing filter. Another
#           option might be to define PWM sensitivity threshold for each servo and not to make adjustments smaller
#           than that.
#
# ----------------------------------------------------------------------------------------------------------------------

import math
import pigpio
import time

from dotmap import DotMap

from util import Util


# ======================================================================================================================
class Servo(object):
    '''
    Servo motor routines.
    
    When instantiated, this class handles a single servo. As such, methods that modify the servo's position return
    'self' to allow chaining.
    
    This class also has several useful static methods. One of them (get_servos) returns all the servos the current
    robotics project uses.
    '''
    
    # All servos:
    PI2 = math.pi / 2
    
    IDEAL_PWM_MIN =  500  # [us]
    IDEAL_PWM_CEN = 1500  # ^
    IDEAL_PWM_MAX = 2500  # ^
    
    IDX_B  = 0  # base
    IDX_NA = 1  # neck azimuth
    IDX_NR = 2  # neck roll
    IDX_SR = 3  # shoulder right
    IDX_SL = 4  # shoulder left
    IDX_ER = 5  # elbow right
    IDX_EL = 6  # elbow left
    # IDX_ = 7
    
    PIN_B  = 21  # 17
    PIN_NA = 20  # 27
    PIN_NR = 16  # 22
    PIN_SR = 12  #  5
    PIN_SL = 24  #  6
    PIN_ER = 23  # 13
    PIN_EL = 18  # 19
    # PIN_ = 26
    
    SERVOS = [
        DotMap(idx=IDX_B,  pin=PIN_B,  pwm=DotMap(min=520, max=2350, cen=1435, cor=   0, sens=10), servo=None, name='Base'      ),
        DotMap(idx=IDX_NA, pin=PIN_NA, pwm=DotMap(min=510, max=2330, cen=1420, cor=   0, sens=10), servo=None, name='Neck A'    ),
        DotMap(idx=IDX_NR, pin=PIN_NR, pwm=DotMap(min=550, max=2440, cen=1495, cor=-480, sens=10), servo=None, name='Neck R'    ),
        DotMap(idx=IDX_SR, pin=PIN_SR, pwm=DotMap(min=500, max=2500, cen=1500, cor=   0, sens=10), servo=None, name='Shoulder R'),
        DotMap(idx=IDX_SL, pin=PIN_SL, pwm=DotMap(min=500, max=2300, cen=1400, cor=   0, sens=10), servo=None, name='Shoulder L'),
        DotMap(idx=IDX_ER, pin=PIN_ER, pwm=DotMap(min=500, max=2500, cen=1500, cor=   0, sens=10), servo=None, name='Elbow R'   ),
        DotMap(idx=IDX_EL, pin=PIN_EL, pwm=DotMap(min=500, max=2500, cen=1500, cor=   0, sens=10), servo=None, name='Elbow L'   )
    ]
    
    # This servo instance (some values are overwritten in the constructor with those from 'SERVOS'):
    PWM_MIN    =  800      # [us]
    PWM_CEN    = 1500      # ^
    PWM_COR    =    0      # ^
    PWM_MAX    = 2200      # ^
    PWM_STEP   =   10      # ^
    PWM_SENS   =   10      # ^
    PWM_DELAY  =    0.010  # [s]
    
    _pi  = None  # TODO: change to 'rpi'
    _idx = -1
    _pin = 0
    _pwm = PWM_CEN
    
    
    # ------------------------------------------------------------------------------------------------------------------
    def __init__(self, pi, idx=-1):
        if pi is None or not pi.connected: return
        
        self._pi  = pi
        self._idx = idx
        
        if (self._idx >= 0 and self._idx < len(self.SERVOS)):
            s = self.SERVOS[self._idx]
            
            self.PWM_MIN    = s.pwm.min
            self.PWM_MAX    = s.pwm.max
            self.PWM_CEN    = s.pwm.cen + s.pwm.cor
            self.PWM_COR    = s.pwm.cor
            self.PWM_SENS   = s.pwm.sens
            
            self._pin = s.pin

        self._set_pwm(self.PWM_CEN)
    
    # ------------------------------------------------------------------------------------------------------------------
    def __del__(self):
        if self._pi.connected:
            self._pi.set_servo_pulsewidth(self._pin, self.PWM_CEN)
    
    # ------------------------------------------------------------------------------------------------------------------
    @staticmethod
    def get_servos(pi):
        ''' Returns the copy of the SERVOS list after initilizing all servos in it. '''
        
        if pi is None or not pi.connected: return None
        
        try:
            S = [s for s in Servo.SERVOS]
            for s in S:
                s.servo = Servo(pi, s.idx)
            return S
        except:
            if self._pi.connected:
                self._pi.stop()
            return None
    
    # ------------------------------------------------------------------------------------------------------------------
    @staticmethod
    def pwm2rad_ideal(pwm):
        ''' Angle: <-90, 90> [deg]'''

        rad = (pwm - Servo.IDEAL_PWM_MIN) / (Servo.IDEAL_PWM_MAX - Servo.IDEAL_PWM_MIN) * math.pi - Servo.PI2
        return Util.val_range(rad, -Servo.PI2, Servo.PI2)
    
    # ------------------------------------------------------------------------------------------------------------------
    @staticmethod
    def rad2pwm_ideal(rad):
        ''' Angle: <-90, 90> [deg]'''

        pwm = ((rad + Servo.PI2) / math.pi * (Servo.IDEAL_PWM_MAX - Servo.IDEAL_PWM_MIN)) + Servo.IDEAL_PWM_MIN
        return Util.val_range(pwd, -Servo.PI2, Servo.PI2)
    
    # ------------------------------------------------------------------------------------------------------------------
    def pwm2rad(self, pwm):
        ''' Angle: <-90, 90> [deg]'''
        
        rad = (pwm - Servo.IDEAL_PWM_MIN) / (Servo.IDEAL_PWM_MAX - Servo.IDEAL_PWM_MIN) * math.pi - Servo.PI2
        return Util.val_range(rad, -Servo.PI2, Servo.PI2)
    
    # ------------------------------------------------------------------------------------------------------------------
    def rad2pwm(self, rad):
        ''' Angle: <-90, 90> [deg]'''
        
        pwm = ((rad + Servo.PI2) / math.pi * (self.PWM_MAX - self.PWM_MIN)) + self.PWM_MIN
        return Util.val_range(pwm, self.PWM_MIN, self.PWM_MAX)
    
    # ------------------------------------------------------------------------------------------------------------------
    def _set_pwm_delay(self, pwm, step_size=PWM_STEP, delay=PWM_DELAY):
        ''' Set PWM in steps with a time delay after each step. Used to move the servo more slowly and more quietly. '''
        
        # Prepare:
        pwm = Util.val_range(pwm, self.PWM_MIN, self.PWM_MAX)
        # if abs(self._pwm - pwm) < self.PWM_SENS: return self
        
        if self._pwm == pwm:
            return self
        elif self._pwm < pwm:  # going left
            n = int(round((pwm - self._pwm) / step_size))
            step = step_size
        else:  # going right
            n = int(round((self._pwm - pwm) / step_size))
            step = -step_size
        
        # Step through:
        for i in range(n):
            self._pwm += step
            self._pi.set_servo_pulsewidth(self._pin, self._pwm)
            time.sleep(delay)
        
        # Set the exact final value:
        self._pwm = pwm
        self._pi.set_servo_pulsewidth(self._pin, self._pwm)
        return self

    # ------------------------------------------------------------------------------------------------------------------
    def _set_pwm(self, pwm):
        pwm = Util.val_range(pwm, self.PWM_MIN, self.PWM_MAX)
        # if abs(self._pwm - pwm) < self.PWM_SENS: return self
        
        self._pwm = pwm
        self._pi.set_servo_pulsewidth(self._pin, self._pwm)
        return self
    
    # ------------------------------------------------------------------------------------------------------------------
    def get_pwm(self):
        return self._pwm

    # ------------------------------------------------------------------------------------------------------------------
    def go_dn(self):
        return self.set_pwm(self._pwm - self.PWM_STEP)
    
    # ------------------------------------------------------------------------------------------------------------------
    def go_max(self, do_delay=False, step_size=PWM_STEP, delay=PWM_DELAY):
        return self.set_pwm(self.PWM_MAX, do_delay, step_size, delay)
    
    # ------------------------------------------------------------------------------------------------------------------
    def go_cen(self, do_delay=False, step_size=PWM_STEP, delay=PWM_DELAY):
        return self.set_pwm(self.PWM_CEN, do_delay, step_size, delay)
    
    # ------------------------------------------------------------------------------------------------------------------
    def go_min(self, do_delay=False, step_size=PWM_STEP, delay=PWM_DELAY):
        return self.set_pwm(self.PWM_MIN, do_delay, step_size, delay)
    
    # ------------------------------------------------------------------------------------------------------------------
    def go_oc(self, pwm, do_delay=False, step_size=PWM_STEP, delay=PWM_DELAY):
        ''' Go off-center. '''
        
        return self.set_pwm(self.PWM_CEN + pwm, do_delay, step_size, delay)
    
    # ------------------------------------------------------------------------------------------------------------------
    def go_to(self, pwm, do_delay=False, step_size=PWM_STEP, delay=PWM_DELAY):
        return self.set_pwm(pwm, do_delay, step_size, t)
    
    # ------------------------------------------------------------------------------------------------------------------
    def go_up(self):
        return self.set_pwm(self._pwm + self.PWM_STEP)
    
    # ------------------------------------------------------------------------------------------------------------------
    def set_pwm(self, pwm, do_delay=False, step_size=PWM_STEP, delay=PWM_DELAY):
        if not do_delay:
            return self._set_pwm(pwm)
        else:
            return self._set_pwm_delay(pwm, step_size, delay)
    
    # ------------------------------------------------------------------------------------------------------------------
    def set_rad(self, rad, do_delay=False, step_size=PWM_STEP, delay=PWM_DELAY):
        return self.set_pwm(self.rad2pwm(rad), do_delay, step_size, delay)


# ======================================================================================================================
if __name__ == "__main__":
    # PWM to radians:
    print(Servo.pwm2rad_ideal( 500))
    print(Servo.pwm2rad_ideal(1000))
    print(Servo.pwm2rad_ideal(1500))
    print(Servo.pwm2rad_ideal(2000))
    print(Servo.pwm2rad_ideal(2500))
    
    # Radians to PWM:
    print(Servo.rad2pwm_ideal(-math.pi / 2))
    print(Servo.rad2pwm_ideal(-math.pi / 4))
    print(Servo.rad2pwm_ideal( 0))
    print(Servo.rad2pwm_ideal( math.pi / 4))
    print(Servo.rad2pwm_ideal( math.pi / 2))
    
    # Servo movement:
    pi = pigpio.pi()
    if not pi.connected: exit(1)
    
    s = Servo(pi, Servo.IDX_SR)  # right shoulder
    s.go_cen(True).go_min(True).go_max(True).go_cen()
    
    time.sleep(0.5)
    s.go_oc(500, True).go_cen()
    
    time.sleep(0.5)
    s.go_oc(-500, True).go_cen()
    
    pi.stop()
