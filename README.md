# Hector
Hector the robot will rule the world!  One day.


## Introduction
This is home of the code that runs Hector the robot.  The project is in a perpetual state of development.  At the moment, the code published here enables teleoperation and telepresence when paired with the [Android app](https://gitlab.com/ubiq-x/hector-android).  Naturally, mechanical and electrical engineering is involved as well.

Single servo control routines are located in `servo_gpio.py`.  Even though the module itslef is robot-flavored, those routines are universal and should be useful for other applications involving servo motors.  The server that allows to remotely connect to the robot is in `server.py`.  Finally, `hector.py` uses a curses-based interface to allow local control of all the robot's servos.  That bit is useful for making sure the setup works and for determining the PWM limits of each individual servo.


## Dependencies
- [Python 3](https://www.python.org)
- [pigpio](https://github.com/joan2937/pigpio)
- [DotMap](https://github.com/drgrib/dotmap)
- [TCP.py](https://gitlab.com/ubiq-x/tcp.py)


## Setup
### Hardware
At least two servos are needed to rotate the robot's head (i.e., pan and tilt).  More servos are present in `servo_gpio.py`, but they aren't used by this repo at this point.  The servos are controlled by a single-board computer (SBC; Raspberry Pi 0W in my case) through its GPIO.  Have a look at the wiki for pictures of the custom PCB I've been using for that purpose.  Additionally, the robot should have two cameras mounted on its head if stereoscopic viewing is desired.  Because most SBCs are slow, I've dedicated a separete one (Raspberry Pi 3) to stream the stereo feeds.  Even with that though the system leaves a lot to be desired in terms of framerate and video responsiveness.

### Software
#### Hector
First, initialize a new Python venv and install all requirements:
```sh
venv=hector
python=python3.6

mkdir -p ~/prj/
cd ~/prj
$python -m venv $venv
cd $venv
source ./bin/activate

python -m pip install pigpio, dotmap

mkdir -p src
cd src
git clone https://github.com/ubiq-x/hector
```

Then, start the server or the local control program:
```sh
cd hector
python server.py
python hector_gpio.py
```

#### Video streaming
For the time being, I've been using a pair of PS Eye3 USB cameras with the MJPEG Streamer on the Raspberry Pi 3 running Raspbian 9 (stretch).  Read on if you'd like to use that as well.  First, prepare the system for streaming:
```sh
# Install:
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install libjpeg-dev cmake
mkdir -p ~/tmp
cd ~/tmp
git clone https://github.com/jacksonliam/mjpg-streamer
make
sudo make install

# Create minimal Web pages with the left and right video feed embedded:
sudo cp -r /usr/local/share/mjpg-streamer/www /usr/local/share/mjpg-streamer/www-l
sudo cp -r /usr/local/share/mjpg-streamer/www /usr/local/share/mjpg-streamer/www-r

sudo echo '<html><body style="margin:0px; background-color: #000;"><center><img src="./?action=stream" style="width: 100%;" /></center></body></html>' >> /usr/local/share/mjpg-streamer/www-l/stream_zero.html

sudo cp /usr/local/share/mjpg-streamer/www-l/stream_zero.html /usr/local/share/mjpg-streamer/www-r
```

Then, start both streams:
```sh
/usr/local/bin/mjpg_streamer -i "input_uvc.so -r 640x480 -d /dev/video0 -f 30 -q 80" -o "output_http.so -p 3301 -w /usr/local/share/mjpg-streamer/www-l"
/usr/local/bin/mjpg_streamer -i "input_uvc.so -r 640x480 -d /dev/video1 -f 30 -q 80" -o "output_http.so -p 3302 -w /usr/local/share/mjpg-streamer/www-r"
```
Naturally, you may want to change the resolution, framerate, quality, and the ports.

Finally, you can check how many clients are connected to either of the ports like so:
```sh
sudo netstat -anp | grep 3301 | grep ESTABLISHED | wc -l
sudo netstat -anp | grep 3302 | grep ESTABLISHED | wc -l
```


### Videos
A few short videos showing the progress on the robot:
- [Hector 01: Android phone orientation test](https://www.youtube.com/watch?v=OKBewA5BHsY)
- [Hector 02: Teleoperation and telepresence](https://www.youtube.com/watch?v=wH5Nxwbf1Uo)
