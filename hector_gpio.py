import curses
import pigpio

from dotmap import DotMap

from servo_gpio import Servo


# ======================================================================================================================
class Hector(object):
    SERVER_PORT = 3333
    
    _pi     = None
    _servos = None
    
    # ------------------------------------------------------------------------------------------------------------------
    def __init__(self):
        try:
            self._pi = pigpio.pi()
            if not self._pi.connected: exit(1)

            self._servos = Servo.get_servos(self._pi)
        except:
            if self._pi.connected:
                self._pi.stop()
    
    # ------------------------------------------------------------------------------------------------------------------
    def __del__(self):
        if self._pi.connected:
            self.servos_center()
            self._pi.stop()
    
    # ------------------------------------------------------------------------------------------------------------------
    def do_local_control(self):
        try:
            ui_x       =  4
            ui_x_val   = 13
            ui_y_tit   =  2
            ui_y_servo =  6
            ui_y_pwm   =  7
            
            servo_idx = 0  # for servo traversal
            servo = self._servos[servo_idx]  # shorthand (note that servo.servo is the actual Servo object)
            servo_pwm = servo.servo.get_pwm()  # for display only as Servo manages its state internally
            
            # (1) Init the UI:
            scr = curses.initscr()
            
            curses.curs_set(0)
            curses.noecho()
            
            curses.start_color()
            curses.init_pair(1, curses.COLOR_WHITE,  curses.COLOR_BLACK)
            curses.init_pair(2, curses.COLOR_GREEN,  curses.COLOR_BLACK)
            curses.init_pair(3, curses.COLOR_YELLOW, curses.COLOR_BLACK)
            curses.init_pair(4, curses.COLOR_RED,    curses.COLOR_BLACK)
            
            scr.box()
            scr.addstr(ui_y_tit,    ui_x,     'HECTOR: LOCAL CONTROL', curses.A_UNDERLINE)
            scr.addstr(ui_y_servo,  ui_x,     'Servo:')
            scr.addstr(ui_y_pwm,    ui_x,     'PWM:          us')
            
            scr.addstr(ui_y_servo,  ui_x_val, '{:16}' .format(servo.name), curses.color_pair(1))
            scr.addstr(ui_y_pwm,    ui_x_val, '{}'    .format(servo_pwm),  curses.color_pair(1))
            scr.refresh()
            
            # (2) Run the control loop:
            self.servos_center()
            while True:
                c = scr.getch()
                
                # (2.1) PWM control:
                if   c == ord('4'): servo.servo.go_dn()
                elif c == ord('6'): servo.servo.go_up()
                elif c == ord('5'): servo.servo.go_cen(True)
                elif c == ord('8'): servo.servo.go_max(True)
                elif c == ord('2'): servo.servo.go_min(True)
                
                # (2.2) Servo selection:
                elif c == ord('7'):
                    if servo_idx > 0:
                        servo_idx -= 1
                        servo = self._servos[servo_idx]
                
                elif c == ord('9'):
                    if servo_idx < len(self._servos) - 1:
                        servo_idx += 1
                        servo = self._servos[servo_idx]
                
                # (2.3) Loop end:
                elif c == ord('q'):
                    break
                
                # (2.4) Refresh the UI:
                servo_pwm = servo.servo.get_pwm()
                scr.addstr(ui_y_servo, ui_x_val, '{:16}' .format(servo.name), curses.color_pair(1))
                scr.addstr(ui_y_pwm,   ui_x_val, '{:4}'  .format(servo_pwm ), curses.color_pair(1))
                scr.refresh()
        finally:
            # (3) Deinit the UI:
            curses.endwin()
            self.servos_center()
    
    # ------------------------------------------------------------------------------------------------------------------
    def servos_center(self):
        for s in self._servos:
            s.servo.go_cen()


# ======================================================================================================================
if __name__ == "__main__":
    import signal
    import sys

    h = Hector()
    h.do_local_control()
